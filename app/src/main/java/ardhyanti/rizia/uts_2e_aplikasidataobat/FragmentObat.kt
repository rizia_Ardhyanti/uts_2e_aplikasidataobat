package ardhyanti.rizia.uts_2e_aplikasidataobat

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_obat.*
import kotlinx.android.synthetic.main.frag_data_obat.view.*

class FragmentObat : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener    {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaKategori = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertObat->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUpdateObat->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnDeleteObat->{
                dialog.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnCari->{
                showDataObat(edNamaObat.text.toString())
            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namaKategori : String=""
    var namaObat : String =""
    var kodeObat : String=""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_obat,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteObat.setOnClickListener(this)
        v.btnInsertObat.setOnClickListener(this)
        v.btnUpdateObat.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.btnCari.setOnClickListener(this)
        v.lsObat.setOnItemClickListener(Clik)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataObat("")
        showDataKategori()

    }
    fun showDataObat(namaObat : String){
        var sql=""
        if(!namaObat.trim().equals("")){
            sql = "select b.kode_obat as _id, b.nama_obat, k.nama_kategori from obat b , kategori k " +
                    " where b.id_kategori=k.id_kategori and b.nama_obat like '%$namaObat%'"
        }else{
            sql = "select b.kode_obat as _id, b.nama_obat, k.nama_kategori from obat b , kategori k " +
                    " where b.id_kategori=k.id_kategori order by b.nama_obat asc"
        }
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_obat,c,
            arrayOf("_id","nama_obat","nama_kategori"), intArrayOf(R.id.txKodeObat,R.id.txNamaObat, R.id.txKategoriObat),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsObat.adapter = lsAdapter

    }

    fun showDataKategori(){
        val c : Cursor = db.rawQuery("select nama_kategori as _id from kategori order by nama_kategori asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    val Clik =  AdapterView.OnItemClickListener{parent, view, position, id ->
        val c : Cursor =  parent.adapter.getItem(position) as Cursor
        kodeObat = c.getString(c.getColumnIndex("_id"))
        v.edNamaObat.setText(c.getString(c.getColumnIndex("nama_obat")))
        v.edKodeObat.setText(c.getString(c.getColumnIndex("_id")))
    }

    fun insertDataObat(kode_obat : String, namaObat: String, id_kategori:Int){
        var sql = "insert into obat(kode_obat,nama_obat,id_kategori) values(?,?,?)"
        db.execSQL(sql, arrayOf(kode_obat,namaObat,id_kategori))
        showDataObat("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kategori from kategori where nama_kategori='$namaKategori'"
        val c:Cursor=db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            insertDataObat(v.edKodeObat.text.toString(),v.edNamaObat.text.toString(),
                c.getInt(c.getColumnIndex("id_kategori")))
            v.edKodeObat.setText("")
            v.edNamaObat.setText("")
        }
    }
    fun updateDataObat(kode_obat:String, namaObat: String,id_kategori: Int){
        var cv : ContentValues = ContentValues()
        cv.put("nama_obat",namaObat)
        cv.put("id_Kategori",id_kategori)
        db.update("obat",cv,"kode_obat= '$kode_obat'",null)
        showDataObat("")

    }
    val btnUpdateDialog=DialogInterface.OnClickListener { dialog, which ->

        var sql = "select id_kategori from kategori where nama_kategori ='$namaKategori'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            updateDataObat(
                v.edKodeObat.text.toString(), v.edNamaObat.text.toString(),
                c.getInt(c.getColumnIndex("id_kategori"))
            )
            v.edKodeObat.setText("")
            v.edNamaObat.setText("")

        }
    }
    fun deleteDataObat(kodeObat : String){
        db.delete(" obat ","kode_obat = '$kodeObat'",null)
        showDataObat("")
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataObat(kodeObat)
        v.edKodeObat.setText("")
        v.edNamaObat.setText("")
    }
}