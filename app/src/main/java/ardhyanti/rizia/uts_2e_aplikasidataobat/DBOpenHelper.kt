package ardhyanti.rizia.uts_2e_aplikasidataobat

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context) : SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tObat = "create table obat(kode_obat text primary key , nama_obat text not null , id_kategori Int not null)"
        val tKtg = "create table kategori(id_kategori integer primary key autoincrement, nama_kategori text not null)"
        val insKtg = "insert into kategori(nama_kategori) values ('Obat Bebas'),('Obat Bebas Terbatas'),('Obat terlarang')"
        db?.execSQL(tObat)
        db?.execSQL(tKtg)
        db?.execSQL(insKtg)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "dataobat"
        val DB_Ver = 4
    }
}